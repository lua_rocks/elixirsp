
```elixir
defp deps do
  [{:exlua, github: "bendiken/exlua", tag: "0.3.0"},
   {:luerl, github: "bendiken/luerl", branch: "exlua",
            compile: "make && cp src/luerl.app.src ebin/luerl.app"}]
end
```

```elixir
defmodule Mixup.Mixfile do
  use Mix.Project

  def project do
    [
      app: :mixup,
      version: "0.0.1",
      elixir: "~> 1.6.3",
      deps: deps(),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod
    ]
  end

  def application do
    [applications: [:logger, :cowboy, :plug]]
  end

  defp deps do
    [
      {:poison, "~> 3.1.0"},
      {:plug, "~> 1.6.0"},
      {:cowboy, "~> 2.4.0"},
      {:credo, "~> 0.5", only: [:dev, :test]},
      {:inch_ex, "~> 1.0", only: :docs}
    ]
  end
end
```

```elixir
# Ecto?
config :app, :dns
  servers: [
    %{
      hostname: "ns1.example.org",
      region: "east",
      type: :bind
    },
    %{
      hostname: "ns2.example.net",
      region: "west",
      type: :nsd4
    }
  ]
```
