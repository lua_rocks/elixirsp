---@diagnostic disable: lowercase-global
package = "elixirsp"
version = "0.1.0-1"

source = {
  url = "git+https://gitlab.com/lua_rocks/elixirsp.git",
  tag = "v0.1.0"
}

description = {
  summary = "An simple parser of elixir configurations exs-files",
  detailed = [[
    Parse exs elixir source code into a lua-table.
  ]],
  homepage = "https://gitlab.com/lua_rocks/elixirsp",
  license = "MIT"
}

dependencies = {
  -- optional "https://gitlab.com/lua_rocks/dprint",
}

build = {
  type = "builtin",
  modules = {
    elixersp = "src/elixersp.lua"
  }
}
