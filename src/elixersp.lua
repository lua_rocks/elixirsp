-- 13-03-2024 @author Swarg
-- Goals: simple elixir parser
--  -- parse elixir code used as config to luatable
--
local M = {}

-- luarocks/dprint: pick up if installed or automatically create a stub
local ok_dprint, D = pcall(require, 'dprint') -- debugging print
if not ok_dprint then
  D = {
    _VERSION = 'stub-of-dprint',
    mk_dprint_for = function() return function() end end,
    mk_dvisualize_for = function() return function() end end,
  }
end
local dprint, dvisual = D.mk_dprint_for(M), D.mk_dvisualize_for(M)

-- spaces count in the indentation
M.tab_cnt = 2
local E = {}

--------------------------------------------------------------------------------
--    Strings Utils
--------------------------------------------------------------------------------

---@param str string
---@param start string ends
function M.starts_with(str, start)
  return str ~= nil and start ~= nil and str:sub(1, #start) == start
end

---@param str string?
---@param e string ends
function M.ends_with(str, e)
  return str ~= nil and e ~= nil and (e == "" or str:sub(- #e) == e)
end

---@param sep string?
---@param t table? out
---@param trim boolean? skip empty words(lines)
function M.split(input, sep, t, trim)
  sep = sep or "\n"
  t = t or {}
  local pos, p_end = 1, #input

  local function add(s, e)
    local line = string.sub(input, s, e) --pos, p - #sep)
    if not trim or line ~= "" then
      t[#t + 1] = line
    end
  end

  -- split to lines with keeping an empty lines
  while true do
    local p = string.find(input, sep, pos, true)
    if not p or p > p_end then
      break
    end

    add(pos, p - #sep)
    pos = p + #sep
  end

  if pos <= p_end then
    add(pos, p_end)
  end

  return t
end

---@param line string
function M.get_tab(line)
  return line and string.match(line, '^%s*')
end

---@param line string
---@param tabc number
---@return boolean
function M.is_tabc(line, tabc)
  assert(type(tabc) == 'number', 'tabc')

  if tabc == 0 and #line == 0 then
    return true
  end

  if tabc <= #line then
    local c = line:sub(tabc + 1, tabc + 1)
    if c ~= " " or c == "" then
      for i = 1, tabc do
        local c0 = line:sub(i, i)
        if c0 ~= " " then return false end
      end
      return true
    end
  end
  return false
end

--
-- "xxABCxx" 3 "ABC" --> true
--
---@param line string
---@param index number
---@param sub string
function M.is_part(line, index, sub)
  return line and sub and index > 0 and #line - index >= #sub - 1 and
      line:sub(index, index + #sub - 1) == sub
end

--------------------------------------------------------------------------------

-- parser handlers
local H = {}
M.handlers = H

--------------------------------------------------------------------------------

-- parse source file to lua table
function M.parse(s)
  local typ = type(s)
  local lines
  if typ == 'string' then
    lines = M.split(s, "\n")
  elseif typ == 'table' then
    lines = s
  else
    error('Not supported input type: ' .. tostring(typ))
  end

  local handler
  local t, i = {}, 0
  handler = H.Global

  while handler and i < #lines do
    i = i + 1
    handler, t = handler(t, lines[i], i)
  end
  _, t = H.EOF(t, i + 1) -- eof

  return t
end

-- LineHandlers


--
-- create a new block (module, function, list, etc..) and bind to parent
--
---@param parent table
---@param childname string name of filed in a parent to bind new child block(node)
---@param lnum number -- line number in the source file of the current line
---@param handler function -- handler for this new child block(node)
---@return function - handler of the child
---@return table - child
local function new_node(parent, childname, lnum, handler)
  assert(type(parent) == 'table', 'parent')
  assert(type(handler) == 'function',
    'handler for ' .. tostring(childname) .. ' at ln:' .. tostring(lnum))

  local child = {
    _up = parent,
    _handler = handler, -- aka type of this node + handler(func) as id
    lnb = lnum,
  }
  if parent[childname] ~= nil then
    error('attempt to override alredy existed field' .. tonumber(childname))
  end
  parent[childname] = child

  return handler, child
end

--
-- back to parent block (switch handler and acc-table)
-- close the scope of the current block (module, heredoc, func, list, etc)
--
-- used to clear child table after exit from coresposed handle
-- aka forget parent
---@param child table
---@param lnum number
---@param override_handler function? optional to override handler from child._up
---@return function
---@return table
local function back(child, lnum, override_handler)
  assert(type(child) == 'table', 'child')
  assert(child._up, 'requered ref to parent')
  local parent0 = child._up
  child.lne = lnum
  -- clear service fields
  child._up, child._tabc, child._handler = nil, nil, nil

  return (override_handler or parent0._handler), parent0
end

--------------------------------------------------------------------------------
--                         Parts of FSM Parser
--------------------------------------------------------------------------------

---@param lnum number?
function H.EOF(t, lnum)
  local root = t._up or t                    -- root
  t._up, t._handler, t._tabc = nil, nil, nil -- clear
  root.lne = lnum
  return nil, root
end

---@param t table? acc-table of the result + current state
---@param line string?
---@return function?  handler
---@return table      t -  result accumulator
---@param lnum number
function H.Global(t, line, lnum)
  if line == nil then -- end of file
    return H.EOF(t, lnum)
  end

  local words = M.split(line, " ", nil, true)
  t = t or {}

  if words[1] == 'defmodule' then
    local modulename = words[2]
    assert(modulename ~= nil and modulename ~= "", 'modulename required')
    t._handler = t._handler or H.Global
    t._up = false -- (root) refs to root of result table
    return new_node(t, modulename, lnum, H.Module)
  end

  return H.Global, t
end

--
-- defmodule
--
---@param t table
---@param line string
---@return function?  handler
---@return table      t -  result accumulator
---@param lnum number
function H.Module(t, line, lnum)
  local tab = M.get_tab(line)
  local wa = M.split(line, " ", nil, true)
  local w, w2 = wa[1], wa[2]
  dprint('Module tab:', tab, w, w2)

  -- @moduledoc ~S"""
  if w == '@moduledoc' then -- heredoc
    if M.ends_with(w2, '"""') then
      return new_node(t, 'moduledoc', lnum, H.AModuleDoc)
    else
      -- in the moduledoc body
    end

    --
  elseif w == '@doc' then -- heredoc
    if M.ends_with(w2, '"""') then
      return new_node(t, 'doc', lnum, H.ADoc)
    else
      -- in the doc body
    end

    -- todo import

    --
  elseif w == 'use' then
    t._use = t._use or {} -- macro
    assert(w2 ~= nil and w2 ~= "", "has name for use")
    t._use[w2] = true     -- todo 'use ModuleName, kw-list params'

    -- Module -> Function
    -- todo def x(), do: .. -- oneliner
  elseif w == 'def' or w == 'defp' then
    local funcname = w2
    t._tabc = #tab -- keep the intent width for function in the module_t
    local h, func_t = new_node(t, funcname, lnum, H.Function)
    if w == 'defp' then func_t.private = true end

    return h, func_t

    --
    -- Module (back-to) -> Global
  elseif w == 'end' and tab == '' then -- end of the module
    return back(t, lnum, H.Global)
  end

  return H.Module, t
end

--
-- process @moduledoc (heredoc)
--
---@param t table
---@param line string
---@return function?  handler
---@return table      t -  result accumulator
function H.AModuleDoc(t, line, lnum)
  local s = line:sub(M.tab_cnt + 1, M.tab_cnt + 3)
  dprint('AModuleDoc tab:', s, line)

  if s == '"""' then -- Heredoc (back-to) -> Module
    return back(t, lnum)
  end

  return H.AModuleDoc, t
end

--
-- process @doc (heredoc)
--
---@param t table
---@param line string
---@return function?  handler
---@return table      t -  result accumulator
function H.ADoc(t, line, lnum)
  local s = line:sub(M.tab_cnt + 1, M.tab_cnt + 3)
  dprint('ADoc Heredoc tab:', s, line)

  if s == '"""' then -- ADoc (back-to) -> Module
    return back(t, lnum)
  end

  return H.ADoc, t
end

--
-- def function
--
---@param func_t table
---@param line string
---@return function?  handler
---@return table      t -  result accumulator
---@param lnum number
function H.Function(func_t, line, lnum)
  local wa = M.split(line, " ", nil, true)
  local w, w2 = wa[1], wa[2]
  dprint('Function tab:', w, w2)

  if w == '[' then                  -- Function -> List
    dprint('Function(ParentList?) -> KVList:')
    func_t._tabc = #M.get_tab(line) -- intent for list in the parent(function|list)
    return new_node(func_t, 'list', lnum, H.KVList)

    -- Function (back-to) -> Module
  elseif w == 'end' and M.is_tabc(line, func_t._up._tabc) then
    return back(func_t, lnum)
  end

  return H.Function, func_t
end

---@param list_t table
---@param line string
---@return function?  handler
---@return table      t -  result accumulator
function H.KVList(list_t, line, lnum)
  dprint('KVList', line)

  if not list_t._up then
    error('no parent in KVList ' .. H.name(list_t._handler))
  end
  local tabc = list_t._up._tabc

  -- KVListHandler (back-to) -> Function(?)
  if M.is_tabc(line, tabc) and M.is_part(line, tabc + 1, "]") then
    dprint('KVList (back) -> Function')
    return back(list_t, lnum)
    --
  else
    local t = list_t
    local ok, k, v = M.parse_kvpair(line)
    if ok and k and v then
      t[#t + 1] = { k, v }
    end
    -- todo if not comma at the end of value multiline...
    -- list_t._tabc = #M.get_tab(line) -- intent for list in the parent(function|list)
    -- local kw_t = bind(list_t, {}, 'kwlist', H.KVList)
    -- return H.KVPair, kw_t
  end

  return H.KVList, list_t
end

--
--
function H.KVPair(t, line)
  if not t._findend then
    local ok, k, v = M.parse_kvpair(line)
    if ok and k and v then
      t[#t + 1] = { k, v }
    end
    -- todo if kv not one-liner then find value
  end

  return H.KVList, t
end

H.func2name = {
  [H.EOF]        = 'EOF',
  [H.Global]     = 'Global',
  [H.Module]     = 'Module',
  [H.AModuleDoc] = 'AModuleDoc',
  [H.ADoc]       = 'ADoc',
  [H.Function]   = 'Function',
  [H.KVList]     = 'KVList',
  [H.KVPair]     = 'KVPair',
}
--
-- debuggin helper
---@param h function?
function H.name(h)
  if h == nil then
    return 'nil'
  else
    return H.func2name[h] or 'Unknown'
  end
end

--------------------------------------------------------------------------------
-- Lexeme parser
--------------------------------------------------------------------------------

-- oneliner to table
-- # {:dep_from_hexpm, "~> 0.3.0"},
-- extra_applications: [:logger]
-- app: :my_app,
-- version: "0.1.0",
-- # elixir: "~> 1.16",
-- # elixirc_paths: ["./lib"]
-- start_permanent: Mix.env() == :prod,
-- deps: deps()

--
---@param line string
function M.parse_kvpair(line)
  local started = nil
  local kvsepi = nil
  local open = nil -- tuple or list or map? { [ %{

  local i, prevc, key, value = 1, nil, nil, nil

  while i <= #line do
    local c = line:sub(i, i)
    dvisual(line, i, i, 'c:', c, 'prevc:', prevc)

    if started then
      if key and c == ',' then
        value = string.sub(line, started, i - 1) -- ', '
        started = i + 1
        --
        -- not key
      elseif c == ' ' and prevc == ':' then    -- ": " key pair separator
        key = string.sub(line, started, i - 2) -- ': '
        started = i + 1
      end
    elseif c ~= ' ' then
      if not started then
        started = i
        if c == '#' then -- comment
          return false, nil, nil
          --
          -- todo [ %{
        elseif c == '{' then
          -- TODO
          -- find end of tuple
          -- parse tuple
        end
      end
    end

    i = i + 1
    prevc = c
  end
  if started and not value then
    value = string.sub(line, started, #line)
  end

  return key ~= nil and value ~= nil, key, value
end

return M
