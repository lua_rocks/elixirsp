require("busted.runner")()
local assert = require("luassert")

local M = require("elixersp");
local H = M.handlers

-- luarocks/dprint: pick up if installed or automatically create a stub
local ok_dprint, D = pcall(require, 'dprint')
if not ok_dprint then
  D = { enable = function() end, disable = function() end }
end
--

local E = {}

local mix_conf_00 = [[
defmodule Some.MixProject do
  use Mix.Project

  def project do
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
  end
end
]]

local mix_conf_01 = [[
defmodule Some.MixProject do
  use Mix.Project

  def project do
    [
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
    ]
  end
end
]]

local mix_conf_02 = [[
defmodule Some.MixProject do
  use Mix.Project

  def project do
    [
      app: :my_app,
      version: "0.1.0",
      # elixir: "~> 1.16",
      # elixirc_paths: ["./lib"]
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
]]

describe("elixer_parser", function()
  before_each(function()
    D.disable()
  end)

  it("split", function()
    assert.same({ 'a', 'b', 'c' }, M.split("a\nb\nc"))
    assert.same({ 'abc', 'deb', '', '', 'f' }, M.split("abc\ndeb\n\n\nf"))
    assert.same({ 'abc', 'deb', '', '', 'f' }, M.split("abc deb   f", " "))
  end)

  it("split trim empty", function()
    assert.same({ 'abc' }, M.split("abc", " ", {}, true))
    assert.same({ 'ab', 'c' }, M.split("ab c", " ", {}, true))
    assert.same({ 'ab', 'c' }, M.split("ab   c", " ", {}, true))
    assert.same({ 'ab', 'c' }, M.split("ab   c  ", " ", {}, true))
    assert.same({ 'abc', 'deb', 'f' }, M.split("abc deb   f", " ", {}, true))
  end)

  it("get_tab", function()
    assert.same("", M.get_tab("def"))
    assert.same('  ', M.get_tab("  def"))
    assert.same('    ', M.get_tab("    def"))
    assert.same('       ', M.get_tab("       "))
  end)


  it("is_tabc", function()
    assert.same(true, M.is_tabc("  x", 2))
    assert.same(true, M.is_tabc("  ", 2))
    assert.same(true, M.is_tabc("   ", 3))
    assert.same(true, M.is_tabc("    ", 4))
    assert.same(true, M.is_tabc("    do", 4))
    assert.same(true, M.is_tabc("    do ", 4))

    assert.same(false, M.is_tabc("\n   do ", 4))
    assert.same(false, M.is_tabc("  do ", 4))
    assert.same(true, M.is_tabc("  do  ", 2))
    assert.same(false, M.is_tabc("do  ", 1))
    assert.same(true, M.is_tabc("do  ", 0))
    assert.same(true, M.is_tabc(" do  ", 1))
    assert.same(false, M.is_tabc(" do  ", 3))
  end)


  it("is_part", function()
    local f = M.is_part
    assert.same('ABC', string.sub("xxxABCxx", 4, 3 + 3))
    assert.same(true, f("xxxABCxx", 4, "ABC"))
    assert.same(true, f("xxxABC", 4, "ABC"))
    assert.same(true, f("ABC", 1, "ABC"))
    assert.same(true, f("A", 1, "A"))
    assert.same(true, f("  ]", 3, "]"))
    assert.same(true, f("  ]x", 3, "]"))
    assert.same(true, f("  ]x", 3, "]x"))
    assert.same(false, f("  ]X", 3, "]x"))
  end)

  -----------------------------------------------------------------------------

  it("name", function()
    local f = H.name
    assert.same('Global', f(H.Global))
    assert.same('Module', f(H.Module))
    assert.same("Function", f(H.Function))
    assert.same("KVList", f(H.KVList))
    assert.same("KVPair", f(H.KVPair))
    assert.same("nil", f(nil))
    assert.same("Unknown", f(function() end))
  end)

  -- require 'dprint'.enable()
  -- D.dprint("\n ", ':dump:', H.func2name, "T", ':dump:', t, "\nEXP", ':dump:', exp)

  it("switch from Global handler to Module handler on defmodule", function()
    local line = "defmodule Some.MixProject do"

    local cb, t = nil, {}
    cb, t = H.Global(t, line, 1) -- t -- mod_t
    assert.same(H.Module, cb)
    local mod_t = { _handler = H.Module, lnb = 1 }
    local glob_t = {
      ['Some.MixProject'] = mod_t, _handler = H.Global, _up = false
    }
    mod_t._up = glob_t -- back from Module to Global
    local exp_mt = mod_t

    assert.same(exp_mt, t)
    cb, t = H.Module(t, "end", 2) -- close defmodule
    assert.same(cb, H.Global)
    local exp_gt = {
      ["Some.MixProject"] = { lnb = 1, lne = 2 },
      _handler = H.Global,
      _up = false,
    }
    assert.same(exp_gt, t)
    cb, t = H.Global(t, nil, 3)                                             -- eof
    assert.same({ ["Some.MixProject"] = { lnb = 1, lne = 2 }, lne = 3 }, t) -- final output
  end)


  it("ignore nested end ino ModuleHandler", function()
    local cb, t
    cb, t = H.Global(t, "defmodule Sm.Mix do", 1)
    assert.same(H.Module, cb)

    local mod_t = { _handler = H.Module, lnb = 1 }
    local glob_t = {
      _up = false, -- root
      _handler = H.Global,
      ['Sm.Mix'] = mod_t,
    }
    mod_t._up = glob_t -- bind
    local exp_mt = mod_t

    assert.same(exp_mt, t)
    cb, t = H.Module(t, "  end", 2) -- irnore
    assert.same(H.Module, cb)
  end)


  it("moduledoc false", function()
    local lines = {
      "defmodule Some.MixProject do",
      "  @moduledoc false",
      "",
      "end",
    }
    local cb, t = nil, {}
    cb, t = H.Global(t, lines[1], 1)
    assert.same(H.Module, cb) ---@cast cb function
    cb, t = cb(t, lines[2]) -- Module -> Heredoc
    -- assert.same('moduledoc false', t.last)
    assert.same(H.Module, cb) ---@cast cb function
  end)


  it("doc false", function()
    local lines = {
      "defmodule Some.MixProject do",
      "  @doc false",
      "end",
    }
    local cb, t = nil, {}
    cb, t = H.Global(t, lines[1], 1)
    assert.same(H.Module, cb) ---@cast cb function
    cb, t = cb(t, lines[2])
    -- assert.same('doc false', t.last)
    assert.same(H.Module, cb) ---@cast cb function
  end)


  it("moduledoc heredoc", function()
    local lines = {
      "defmodule Some.MixProject do", -- 1
      "  @moduledoc \"\"\"",          -- 2 heredoc start
      "  some documentations",        -- 3
      "  lines",                      -- 4
      "end",                          -- 5 ignore
      '  """',                        -- 6 heredoc end
      "end",                          -- 7 module end
    }
    local cb, t
    cb, t = H.Global(nil, lines[1], 1)
    assert.same(H.Module, cb) ---@cast cb function

    cb, t = cb(t, lines[2], 2)
    assert.same(H.AModuleDoc, cb) ---@cast cb function

    -- skip heredoc body
    for i = 3, 5 do
      cb, t = cb(t, lines[i], i)
      assert.same(H.AModuleDoc, cb) ---@cast cb function
    end

    cb, t = cb(t, lines[6]) -- Heredoc (back-to) Module
    assert.same(H.Module, cb) ---@cast cb function

    cb, t = cb(t, lines[7])
    assert.same(H.Global, cb) ---@cast cb function

    cb, t = cb(t, lines[8]) -- end of the file lines[8] is nil
    assert.same(nil, cb)
  end)


  it("(fn)doc Heredoc", function()
    local lines = {
      "defmodule Some.MixProject do", -- 1
      "  @doc \"\"\"",                -- 2 heredoc start
      "  some documentations",        -- 3
      "  for function",               -- 4
      "end",                          -- 5 ignore
      '  """',                        -- 6 heredoc end
      -- def func_name do
      -- ...
      -- end
      "end", -- 7 module end
    }
    local cb, t = nil, {}
    cb, t = H.Global(t, lines[1], 1)
    assert.same(H.Module, cb) ---@cast cb function

    cb, t = cb(t, lines[2], 2)
    -- assert.same('doc heredoc', st.last)
    assert.same(H.ADoc, cb) ---@cast cb function

    -- skip heredoc body
    for i = 3, 5 do
      cb, t = cb(t, lines[i])
      assert.same(H.ADoc, cb) ---@cast cb function
    end

    cb, t = cb(t, lines[6])
    assert.same(H.Module, cb) ---@cast cb function

    cb, t = cb(t, lines[7])
    assert.same(H.Global, cb) ---@cast cb function

    cb, t = cb(t, lines[8]) -- end of the file lines[8] is nil
    assert.same(nil, cb)
  end)


  it("Function handler", function()
    local lines = {
      "defmodule Some.MixProject do", --  1
      "  def project do",             --  2
      "    # ...",                    --  3
      "  end",                        --  4 end of function
      '  ',                           --  5
      "end",                          --  6 module end
    }
    local cb, t = nil, {}
    cb, t = H.Global(t, lines[1], 1) -- Glob -> Module
    assert.same(H.Module, cb) ---@cast cb function
    cb, t = cb(t, lines[2], 2)       -- Module -> Func
    assert.same(H.Function, cb)

    -- process func body
    cb, t = cb(t, lines[3], 3) -- in Function
    assert.same(H.Function, cb) ---@cast cb function

    cb, t = cb(t, lines[4], 4) -- Function (back-to) --> Module
    assert.same(H.Module, cb) ---@cast cb function

    cb, t = cb(t, lines[5], 5)
    assert.same(H.Module, cb) ---@cast cb function

    cb, t = cb(t, lines[6], 6)
    assert.same(H.Global, cb) ---@cast cb function

    cb, t = cb(t, lines[7], 7)
    assert.is_nil(cb) -- finished

    -- module with one function
    local exp = {
      ["Some.MixProject"] = { -- module
        lnb = 1,
        lne = 6,
        project = { -- function
          lnb = 2,
          lne = 4,
        }
      },
      lne = 7
    }

    assert.same(exp, t)
  end)


  it("Function (back-to) -> Module", function()
    local glob_t = {}
    local mod_t = {
      _handler = H.Module,
      _up = glob_t,
      _tabc = 2
    }
    glob_t["Some.MixProject"] = mod_t -- in global scope kv of modules
    local func_t = {
      fn = 'project',
      _up = mod_t,
    }

    local cb, t = H.Function(func_t, '  end', 2)
    assert.same('Module', H.name(cb))
    assert.same(mod_t, t)
  end)


  it("KVList (back-to) -> Function", function()
    local glob_t = {}
    local mod_t = {
      _handler = H.Module,
      _up = glob_t,
      _tabc = 2
    }
    glob_t["Some.MixProject"] = mod_t -- in global scope kv of modules
    local func_t = {
      fn = 'project',
      _handler = H.Function,
      _up = mod_t,
      _tabc = 4
    }
    local kvl_t = {
      _up = func_t
    }

    local cb, t = H.KVList(kvl_t, '    ]') -- triggers back to Function
    assert.same('Function', H.name(cb))
    assert.same(func_t, t)
  end)


  it("FunctionHandler -> KVListHandler", function()
    local lines = {
      "defmodule Some.MixProject do",               --  1
      "  def project do",                           --  2
      '    [',                                      --  3
      '      app: :my_app,',                        --  4
      '      version: "0.1.0",',                    --  5
      '      # elixir: "~> 1.16",',                 --  6
      '      # elixirc_paths: ["./lib"]',           --  7
      '      start_permanent: Mix.env() == :prod,', --  8
      '      deps: deps()',                         --  9
      '    ]',                                      -- 10
      "  end",                                      -- 11 end of function
      '  ',                                         -- 12
      "end",                                        -- 13 module end
    }
    local cb, mod_t, func_t, list_t, kvpair_t, t

    cb, mod_t = H.Global({}, lines[1], 1) -- Glob -> Module
    assert.same(H.Module, cb) ---@cast cb function
    assert.same('Global', H.name(mod_t._up._handler))

    cb, func_t = cb(mod_t, lines[2], 2)           -- Module -> Func
    assert.same(H.Function, cb)                   -- current handler
    assert.equal(mod_t, func_t._up)               -- parent of this function (module)
    assert.equal(2, func_t._up._tabc)
    assert.equal(H.Module, func_t._up._handler)   -- prev handler

    cb, list_t = cb(func_t, lines[3], 2)          -- Func -> KVList
    assert.same(H.KVList, cb)
    assert.equal(func_t, list_t._up)              --
    assert.equal(H.Function, list_t._up._handler) --
    assert.equal(4, (list_t._up or E)._tabc)

    cb, t = cb(list_t, lines[4], 4)
    assert.same(H.KVList, cb)

    cb, t = cb(t, lines[10], 10) -- back KVList -> Function
    assert.same(H.Function, cb)

    cb, t = cb(t, lines[11], 11) -- back Function -> Module
    assert.same(H.Module, cb)

    cb, t = cb(t, lines[13], 13) -- back Module -> Global
    assert.same(H.Global, cb)    --  H.name(cb))

    cb, t = cb(t, lines[14], 14) -- eof
    assert.same(nil, cb)
  end)


  it("success mix_conf_00", function()
    local exp = {
      ["Some.MixProject"] = { -- module name
        _use = { ["Mix.Project"] = true },
        -- functions:
        application = { lnb = 8, lne = 9 },
        project = { lnb = 4, lne = 5 },
        deps = { private = true, lnb = 12, lne = 13 },
        lnb = 1,
        lne = 14,
      },
      lne = 15
    }
    local t = M.parse(mix_conf_00)
    assert.same(exp, t)
  end)


  -- module inside function used to back from functionHandler to ModuleHandler
  it("success", function()
    local exp = {
      ["Some.MixProject"] = {
        -- macro use '_' to avoid name conflict with funcname (def use do)
        _use = { ["Mix.Project"] = true },
        -- functions:
        application = { lnb = 10, lne = 13, list = { lnb = 11, lne = 12 } },
        project = { lnb = 4, lne = 7, list = { lnb = 5, lne = 6 } },
        deps = { private = true, lnb = 16, lne = 19, list = { lnb = 17, lne = 18 } },
        lnb = 1,
        lne = 20,
      },
      lne = 21
    }

    local t = M.parse(mix_conf_01)
    assert.same(exp, t)
  end)

  -- oneliner to table
  -- # {:dep_from_hexpm, "~> 0.3.0"},
  -- extra_applications: [:logger]
  -- app: :my_app,
  -- version: "0.1.0",
  -- # elixir: "~> 1.16",
  -- # elixirc_paths: ["./lib"]
  -- start_permanent: Mix.env() == :prod,
  -- deps: deps()
  it("parse_kvpair", function()
    local f = function(line)
      local ok, k, v = M.parse_kvpair(line)
      return (ok == true and '+' or '-') .. '|' .. tostring(k) .. '|' .. tostring(v)
    end

    assert.same('-|nil|nil', f('#'))
    assert.same('-|nil|nil', f('  #'))
    assert.same('-|nil|nil', f('    #'))
    assert.same('-|nil|nil', f('    # key: :value,'))

    -- require'dprint'.enable()
    assert.same('+|key|42', f('key: 42'))
    assert.same('+|key|42', f('key: 42,'))
    assert.same('+|key|42', f('key: 42, '))
    assert.same('+|key|42', f('key: 42,  '))
    assert.same('+|key|:value', f('key: :value,'))
    assert.same('+|key|:value', f('key: :value,'))
    assert.same('+|version|"0.1.0"', f('version: "0.1.0",'))
    assert.same('+|elixirc_paths|["./lib"]', f('elixirc_paths: ["./lib"]'))
    assert.same('+|extra_applications|[:logger]', f('extra_applications: [:logger]'))

    --
    assert.same('+|deps|deps()', f('deps: deps()'))
    assert.same('+|start_permanent|Mix.env() == :prod', f('start_permanent: Mix.env() == :prod,'))

    -- todo:
    assert.same('-|nil|{:key, :value},', f('{:key, :value},'))

    assert.same('+|key|"va', f('key: "va, lue"'))
    assert.same('+|key| lue"', f('key: "va, lue",'))

    assert.same('-|nil|{:"key with space", :value},', f('{:"key with space", :value},'))
    assert.same('-|nil|:"key with space", :value,', f(':"key with space", :value,'))
  end)


  it("success", function()
    local exp2 = {
      ["Some.MixProject"] = {
        _use = { ["Mix.Project"] = true },
        application = {
          list = { { "extra_applications", "[:logger]" }, lnb = 17, lne = 19 },
          lnb = 16,
          lne = 20,
        },
        deps = {
          list = { lne = 27, lnb = 24 },
          lnb = 23,
          lne = 28,
          private = true
        },
        lne = 29,
        lnb = 1,
        project = {
          list = {
            { "app",             ":my_app" },
            { "version",         '"0.1.0"' },
            { "start_permanent", "Mix.env() == :prod" },
            { "deps",            "deps()" },
            lnb = 5,
            lne = 12,
          },
          lnb = 4,
          lne = 13,
        }
      },
      lne = 30
    }
    local t = M.parse(mix_conf_02)
    assert.same(exp2, t)
  end)
end)
